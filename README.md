#README#

Console Tenant Rest API.

## Running the application ##
	gradle bootRun

## API Methods ##
	#Create Tenant
	POST /api/tenants
		{"name":"Ben","weeklyRent":150}
	
	#Get tenant by ID
	GET /api/tenants/1
	
	#Create Rent Receipt for a tenant
	PATCH /api/tenants/1
		{"amount":100}
	
	#List Rent Receipts for a tenant
	GET /api/tenants/1/receipts
	
	#List all Tenants ​which have a Rent Receipt t​ hat was created within the last “N” hours
	GET /api/tenants?paidHoursAgo=2