package console.dao

import console.domain.RentReceipt
import console.domain.Tenant
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsCollectionWithSize.hasSize
import org.hamcrest.collection.IsEmptyCollection
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import java.math.BigDecimal
import java.time.LocalDateTime


@RunWith(SpringRunner::class)
@DataJpaTest
open class TenantRepositoryTest(){
	
	@Autowired
	lateinit var entityManager: TestEntityManager

	@Autowired
	lateinit var repository: TenantRepository
	
	@Test
	fun findRecentlyPaid_ShouldFindNone() {
		val paidOn = LocalDateTime.now()
		tenantPaidOn(Tenant("Joe", BigDecimal("123.456")), paidOn)
		
		val tenants:List<Tenant> = repository.findRecentlyPaid(paidOn.plusSeconds(1))
		
		assertThat(tenants, IsEmptyCollection.empty())
	}
	
	@Test
	fun findRecentlyPaid_ShouldFindTenant() {
		val paidOn = LocalDateTime.now()
		val tenant = tenantPaidOn(Tenant("Joe", BigDecimal("123.456")), paidOn)
		
		val tenants:List<Tenant> = repository.findRecentlyPaid(paidOn)
		
		assertThat(tenants, hasSize(1))
		assertThat(tenants, contains(tenant))
	}
	
	@Test
	fun findRecentlyPaid_ShouldFindOne() {
		val paidOn = LocalDateTime.now()
		val tenant1 = tenantPaidOn(Tenant("Joe", BigDecimal("123.456")), paidOn)
		tenantPaidOn(Tenant("Ben", BigDecimal("100")), paidOn.minusSeconds(1))
		
		val tenants:List<Tenant> = repository.findRecentlyPaid(paidOn)
		assertThat(tenants, hasSize(1))
		assertThat(tenants, contains(tenant1))
	}
	
	@Test
	fun findRecentlyPaid_ShouldFindBoth() {
		val paidOn = LocalDateTime.now()
		val tenant1 = tenantPaidOn(Tenant("Joe", BigDecimal("123.456")), paidOn)
		val tenant2 = tenantPaidOn(Tenant("Ben", BigDecimal("100")), paidOn.plusSeconds(1))
		
		val tenants:List<Tenant> = repository.findRecentlyPaid(paidOn)
		assertThat(tenants, hasSize(2))
		assertThat(tenants, contains(tenant1, tenant2))
	}

	private fun tenantPaidOn(tenant: Tenant, paidOn: LocalDateTime):Tenant {
		val receipt = RentReceipt()
		receipt.amount = BigDecimal("111.222")
		receipt.tenant = tenant
		receipt.createdDate = paidOn
		tenant.rentReceipts.add(receipt)
		return entityManager.persist(tenant)
	}
}