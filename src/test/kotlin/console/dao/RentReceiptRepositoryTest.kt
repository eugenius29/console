package console.dao

import console.domain.RentReceipt
import console.domain.Tenant
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsCollectionWithSize.hasSize
import org.hamcrest.collection.IsEmptyCollection
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.junit4.SpringRunner
import java.math.BigDecimal
import java.time.LocalDateTime


@RunWith(SpringRunner::class)
@DataJpaTest
open class RentReceiptRepositoryTest() {

	@Autowired
	lateinit var entityManager: TestEntityManager

	@Autowired
	lateinit var repository: RentReceiptRepository

	@Test
	fun findByTenantId_ShouldFindNone() {
		val tenant = entityManager.persist(Tenant("Joe", BigDecimal("123.456")))
		addRentReceipt(tenant, BigDecimal("100"))

		val rentReceipts: List<RentReceipt> = repository.findByTenantId(tenant.id + 1)

		assertThat(rentReceipts, IsEmptyCollection.empty())
	}

	@Test
	fun findRecentlyPaid_ShouldFindReceipt() {
		val tenant = entityManager.persist(Tenant("Joe", BigDecimal("123.456")))
		var receipt1 = addRentReceipt(tenant, BigDecimal("100"))
		var receipt2 = addRentReceipt(tenant, BigDecimal("200"))

		val rentReceipts: List<RentReceipt> = repository.findByTenantId(tenant.id)

		assertThat(rentReceipts, hasSize(2))
		assertThat(rentReceipts, contains(receipt1, receipt2))
	}

	private fun addRentReceipt(tenant: Tenant, amount: BigDecimal): RentReceipt {
		val receipt = RentReceipt()
		receipt.amount = amount
		receipt.tenant = tenant
		receipt.createdDate = LocalDateTime.now()
		return entityManager.persist(receipt)
	}
}