package console.serviceImpl

import console.dao.RentReceiptRepository
import console.dao.TenantRepository
import console.domain.RentReceipt
import console.domain.Tenant
import console.rest.TenantController
import console.service.TenantService
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasProperty
import org.hamcrest.Matchers.hasSize
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.runners.MockitoJUnitRunner
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate
import java.util.ArrayList

@RunWith(MockitoJUnitRunner::class)
class TenantServiceImplTest {

	@Mock
	lateinit var tenantRepository: TenantRepository
	@Mock
	lateinit var rentReceiptRepository: RentReceiptRepository

	lateinit var tenantService: TenantService

	@Mock
	lateinit var tenantMock: Tenant
	@Mock
	lateinit var tenantReceipts: List<RentReceipt>
	@Mock
	lateinit var recentlyPaidTenants: List<Tenant>

	private fun <T> any(): T {
		Mockito.any<T>()
		return uninitialized()
	}

	private fun <T> uninitialized(): T = null as T

	@Before
	fun setUp() {
		MockitoAnnotations.initMocks(this)
		tenantService = TenantServiceImpl(tenantRepository = tenantRepository, rentReceiptRepository = rentReceiptRepository)
	}

	@Test
	fun testSave() {
		val beforeSave = TenantController.TenantCmd("Joe", BigDecimal("300")).toTenant()
		`when`(tenantRepository.save(beforeSave)).thenReturn(tenantMock)

		val tenant = tenantService.save(beforeSave)

		assertThat(tenant, `is`(tenantMock))
		verify(tenantRepository, times(1)).save(beforeSave)
	}

	@Test(expected = ServiceException::class)
	fun testFindOne_notFound() {
		val tenantId = 1L;
		`when`(tenantRepository.findOne(tenantId)).thenThrow(ServiceException::class.java)

		tenantService.findOne(tenantId)
	}

	@Test
	fun testFindOne() {
		val tenantId = 1L;
		`when`(tenantRepository.findOne(tenantId)).thenReturn(tenantMock)

		val tenant = tenantService.findOne(tenantId)

		assertThat(tenant, `is`(tenantMock))
		verify(tenantRepository, times(1)).findOne(tenantId)
	}

	@Test
	fun testPayRent() {
		val tenantId = 1L;
		val paidTo = LocalDate.now()
		tenantMock.weeklyRent = BigDecimal("300")
		tenantMock.credit = BigDecimal.ZERO
		tenantMock.paidTo = paidTo
		tenantMock.rentReceipts = ArrayList()
		`when`(tenantRepository.findOne(tenantId)).thenReturn(tenantMock)
		`when`(tenantRepository.save(tenantMock)).thenReturn(tenantMock)


		var rentReceipt = RentReceipt()
		rentReceipt.amount = BigDecimal("275")

		var tenant = tenantService.payRent(tenantId, rentReceipt)

		assertThat(tenant, `is`(tenantMock))
		assertThat(tenant, allOf(
				hasProperty("credit", equalTo(rentReceipt.amount.setScale(2, RoundingMode.HALF_UP)))
				, hasProperty("paidTo", equalTo(paidTo))
		))
		assertThat(tenant.rentReceipts, hasSize(1))
		assertThat(tenant.rentReceipts, contains(
				hasProperty("amount", equalTo(rentReceipt.amount.setScale(2, RoundingMode.HALF_UP)))
		))

		var rentReceipt2 = RentReceipt()
		rentReceipt2.amount = BigDecimal("50")

		tenant = tenantService.payRent(tenantId, rentReceipt2)

		assertThat(tenant, `is`(tenantMock))
		assertThat(tenant, allOf(
				hasProperty("credit", equalTo(BigDecimal("25").setScale(2, RoundingMode.HALF_UP)))
				, hasProperty("paidTo", equalTo(paidTo.plusWeeks(1L)))
		))
		assertThat(tenant.rentReceipts, hasSize(2))
		assertThat(tenant.rentReceipts, contains(
				hasProperty("amount", equalTo(rentReceipt.amount.setScale(2, RoundingMode.HALF_UP)))
				, hasProperty("amount", equalTo(rentReceipt2.amount.setScale(2, RoundingMode.HALF_UP)))
		))

		verify(tenantRepository, times(2)).findOne(tenantId)
		verify(tenantRepository, times(2)).save(tenantMock)
	}

	@Test
	fun testFindReceipts() {
		val tenantId = 1L;
		`when`(rentReceiptRepository.findByTenantId(tenantId)).thenReturn(tenantReceipts)

		val receipts = tenantService.findReceipts(tenantId)

		assertThat(receipts, `is`(tenantReceipts))
		verify(rentReceiptRepository, times(1)).findByTenantId(tenantId)
	}

	@Test
	fun testFindRecentlyPaid() {
		`when`(tenantRepository.findRecentlyPaid(any())).thenReturn(recentlyPaidTenants)

		val tenants = tenantService.findRecentlyPaid(1)

		assertThat(tenants, `is`(recentlyPaidTenants))
		verify(tenantRepository, times(1)).findRecentlyPaid(any())
	}
}