package console.rest

import console.domain.RentReceipt
import console.domain.Tenant
import console.service.TenantService
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.greaterThan
import org.hamcrest.Matchers.hasProperty
import org.hamcrest.Matchers.hasSize
import org.hamcrest.collection.IsEmptyCollection
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.test.context.junit4.SpringRunner
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDateTime
import java.util.ArrayList
import java.util.Arrays

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class TenantControllerTest {
	//@MockBean
	@Autowired
	lateinit var tenantService: TenantService

	@Autowired
	lateinit var restTemplate: TestRestTemplate

	@Before
	fun setUp() {
		restTemplate.restTemplate.requestFactory = HttpComponentsClientHttpRequestFactory()
	}

	
	@Test
	fun testTenantApi() {
		//no tenant with 1
		var response: ResponseEntity<Tenant> = restTemplate.getForEntity("/tenants/1", Tenant::class.java)
		assertThat(response.statusCode, `is`(HttpStatus.NOT_FOUND))
		
		//create tenant
		val request = TenantController.TenantCmd("Joe", BigDecimal("300"))
		val tenantMock = request.toTenant()
		response = restTemplate.postForEntity("/tenants", request, Tenant::class.java)
		var tenant = response.body
		assertThat(response.statusCode, `is`(HttpStatus.OK))
		assertThat(tenant, allOf(
				hasProperty("name", equalTo(tenantMock.name)),
				hasProperty("weeklyRent", equalTo(tenantMock.weeklyRent)),
				hasProperty("credit", equalTo(tenantMock.credit)),
				hasProperty("id", greaterThan(0L)),
				hasProperty("rentReceipts", equalTo(tenantMock.rentReceipts)),
				hasProperty("paidTo", equalTo(tenantMock.paidTo))
		)
		)
		
		//get tenant by id
		response = restTemplate.getForEntity("/tenants/{id}", Tenant::class.java, tenant.id )
		val tenantFromGet = response.body
		assertThat(response.statusCode, `is`(HttpStatus.OK))
		assertThat(tenantFromGet, allOf(
				hasProperty("name", equalTo(tenant.name)),
				hasProperty("weeklyRent", equalTo(tenant.weeklyRent)),
				hasProperty("credit", equalTo(tenant.credit)),
				hasProperty("id", equalTo(tenant.id)),
				hasProperty("rentReceipts", equalTo(tenant.rentReceipts)),
				hasProperty("paidTo", equalTo(tenant.paidTo))
		)
		)
		
		//create rentReceipt
		val rentReceipt1 = RentReceipt()
		rentReceipt1.amount = BigDecimal("50")
		rentReceipt1.createdDate = LocalDateTime.now()
		response = restTemplate.exchange("/tenants/{id}", HttpMethod.PATCH, HttpEntity(rentReceipt1), Tenant::class.java, tenant.id)
		val tenantWithRentReceipt1 = response.body
		assertThat(tenantWithRentReceipt1, allOf(
				hasProperty("name", equalTo(tenant.name)),
				hasProperty("weeklyRent", equalTo(tenant.weeklyRent)),
				hasProperty("credit", equalTo(rentReceipt1.amount.setScale(2,RoundingMode.HALF_UP))),
				hasProperty("id", equalTo(tenant.id)),
				hasProperty("rentReceipts", equalTo(tenant.rentReceipts)),
				hasProperty("paidTo", equalTo(tenant.paidTo))
		)
		)
		
		//create rentReceipt2
		val rentReceipt2 = RentReceipt()
		rentReceipt2.amount = BigDecimal("275")
		rentReceipt2.createdDate = LocalDateTime.now()
		response = restTemplate.exchange("/tenants/{id}", HttpMethod.PATCH, HttpEntity(rentReceipt2), Tenant::class.java, tenant.id)
		val tenantWithRentReceipt2 = response.body
		assertThat(tenantWithRentReceipt2, allOf(
				hasProperty("name", equalTo(tenant.name)),
				hasProperty("weeklyRent", equalTo(tenant.weeklyRent)),
				hasProperty("credit", equalTo(BigDecimal("25").setScale(2,RoundingMode.HALF_UP))),
				hasProperty("id", equalTo(tenant.id)),
				hasProperty("rentReceipts", equalTo(tenant.rentReceipts)),
				hasProperty("paidTo", equalTo(tenant.paidTo.plusWeeks(1L)))
		)
		)
		
		//get all receipts for tenant
		var receiptsResponse = restTemplate.getForEntity("/tenants/{id}/receipts", Array<RentReceipt>::class.java, tenant.id )
		val receipts = receiptsResponse.body
		assertThat(receipts.size, `is`(2))
		assertThat(receipts.asList(), contains(
			hasProperty("amount", equalTo(rentReceipt1.amount.setScale(2,RoundingMode.HALF_UP)))
			,hasProperty("amount", equalTo(rentReceipt2.amount.setScale(2,RoundingMode.HALF_UP)))
		))
		
		//List all Tenants ​which have a Rent Receipt t​ hat was created within the last N hours
		var tenantsResponse = restTemplate.getForEntity("/tenants?paidHoursAgo={hours}", ArrayList::class.java, 0)
		var tenants = tenantsResponse.body
		assertThat(tenants, IsEmptyCollection.empty())
		
		tenantsResponse = restTemplate.getForEntity("/tenants?paidHoursAgo={hours}", ArrayList::class.java, 1)
		tenants = tenantsResponse.body
		assertThat(tenants, hasSize(1))
	}
	
	
	
}