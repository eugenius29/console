package console.serviceImpl

import console.dao.RentReceiptRepository
import console.dao.TenantRepository
import console.domain.RentReceipt
import console.domain.Tenant
import console.service.TenantService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.RoundingMode
import java.time.LocalDateTime

@Service
@Transactional
open class TenantServiceImpl(
		val tenantRepository: TenantRepository
		, val rentReceiptRepository: RentReceiptRepository) : TenantService {

	override fun save(tenant: Tenant): Tenant = tenantRepository.save(tenant)

	override fun findOne(id: Long): Tenant = tenantRepository.findOne(id)
			?: throw ServiceException("Tenant with id $id not found")
	
	override fun payRent(id: Long, rentReceipt: RentReceipt): Tenant {
		val tenant = findOne(id)
		rentReceipt.amount = rentReceipt.amount.setScale(2,RoundingMode.HALF_UP)
		val paid = tenant.credit.add(rentReceipt.amount)
		val (weeksAdvanced, credit) = paid.divideAndRemainder(tenant.weeklyRent)
		tenant.paidTo = tenant.paidTo.plusWeeks(weeksAdvanced.toLong())
		tenant.credit = credit
		rentReceipt.tenant = tenant
		tenant.rentReceipts.add(rentReceipt)
		return save(tenant)
	}

	override fun findReceipts(id: Long): List<RentReceipt> = rentReceiptRepository.findByTenantId(id)

	override fun findRecentlyPaid(hours: Int): List<Tenant> =
			tenantRepository.findRecentlyPaid(LocalDateTime.now().minusHours(hours.toLong()))
}