package console.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.validation.constraints.NotNull

@Entity
@EntityListeners(AuditingEntityListener::class)
open class RentReceipt() {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	var id: Long = 0
	
	@NotNull
	lateinit var amount: BigDecimal
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@NotNull
	lateinit var tenant: Tenant
	
	@CreatedDate
	@NotNull
	lateinit var createdDate: LocalDateTime
}