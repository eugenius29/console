package console.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate
import java.util.ArrayList
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.validation.constraints.NotNull

@Entity
open class Tenant() {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	var id: Long = 0
	
	@NotNull
	lateinit var name: String
	
	@NotNull
	lateinit var weeklyRent: BigDecimal
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull
	var paidTo: LocalDate = LocalDate.now()
	
	@NotNull
	var credit: BigDecimal = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP)
	
	@OneToMany(mappedBy = "tenant", fetch=FetchType.LAZY, cascade=arrayOf(CascadeType.ALL), orphanRemoval = true)
	@JsonIgnore
	@NotNull
	var rentReceipts: MutableList<RentReceipt> = ArrayList()

	constructor(name: String, weeklyRent: BigDecimal) : this() {
		this.name = name
		this.weeklyRent = weeklyRent.setScale(2,RoundingMode.HALF_UP)
	}
}