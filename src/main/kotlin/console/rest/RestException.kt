package console.rest

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

class RestException {

	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	class NotFound(message: String, ex: Exception? = null) : RuntimeException(message, ex)
}