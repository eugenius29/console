package console.rest

import console.domain.RentReceipt
import console.domain.Tenant
import console.service.TenantService
import console.serviceImpl.ServiceException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.math.RoundingMode


@RestController
class TenantController @Autowired constructor(val tenantService: TenantService) {

	data class TenantCmd(val name: String, val weeklyRent: BigDecimal){
		fun toTenant(): Tenant = Tenant(name = name, weeklyRent = weeklyRent.setScale(2, RoundingMode.HALF_UP))	
	}
	
	@PostMapping("/tenants")
	fun post(@RequestBody tenantCmd: TenantCmd): Tenant = tenantService.save(tenantCmd.toTenant())

	@GetMapping("/tenants/{id}")
	fun get(@PathVariable id: Long): Tenant {
		return try {
			tenantService.findOne(id)
		} catch(e: ServiceException) {
			throw RestException.NotFound(e.message ?: "No tenant with id $id found")
		}
	}

	@PatchMapping("/tenants/{id}")
	fun payRent(@PathVariable id: Long, @RequestBody rentReceipt: RentReceipt): Tenant {
		return tenantService.payRent(id, rentReceipt)
	}

	@GetMapping("/tenants/{id}/receipts")
	fun findReceipts(@PathVariable id: Long): List<RentReceipt> {
		return try {
			tenantService.findReceipts(id)
		} catch(e: ServiceException) {
			throw RestException.NotFound(e.message ?: "No receipts for tenant $id found")
		}
	}

	@GetMapping("/tenants", params = arrayOf("paidHoursAgo"))
	fun findRecentlyPaid(@RequestParam("paidHoursAgo") hours: Int): List<Tenant> {
		return try {
			tenantService.findRecentlyPaid(hours)
		} catch(e: ServiceException) {
			throw RestException.NotFound(e.message ?: "No tenants paid in the last $hours hours")
		}
	}
}