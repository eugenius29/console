package console.service

import console.domain.RentReceipt
import console.domain.Tenant

interface TenantService {
	fun save(tenant: Tenant): Tenant
	fun findOne(id: Long): Tenant
	fun payRent(id: Long, rentReceipt: RentReceipt): Tenant
	fun findReceipts(id: Long): List<RentReceipt>
	fun findRecentlyPaid(hours: Int): List<Tenant>
}