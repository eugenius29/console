package console.dao

import console.domain.Tenant
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.time.LocalDateTime

interface TenantRepository : JpaRepository<Tenant, Long> {

	@Query("select t from Tenant t where exists(select r from RentReceipt r where r.tenant = t and r.createdDate >= ?1)")
	fun findRecentlyPaid(time: LocalDateTime): List<Tenant>
}