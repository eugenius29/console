package console.dao

import console.domain.RentReceipt
import org.springframework.data.jpa.repository.JpaRepository

interface RentReceiptRepository : JpaRepository<RentReceipt, Long> {
	
	fun findByTenantId(id:Long) : List<RentReceipt>
}